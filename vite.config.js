import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';
import vue from '@vitejs/plugin-vue';

// vite.config.js / vite.config.ts
import { viteStaticCopy } from 'vite-plugin-static-copy'
import path from 'path'

export default defineConfig({
    plugins: [
        laravel({
            input: [
                'resources/sass/app.scss',
                'resources/js/app.js',
            ],
            refresh: true,
            //publicDirectory: "public_html",
        }),
        vue({
            template: {
                transformAssetUrls: {
                    base: null,
                    includeAbsolute: false,
                },
            },
        }),
        viteStaticCopy({
            targets: [
                {
                    //src: path.resolve(__dirname, './resources/img') + '/[!.]*',
                    //dest: './public/img/',

                    src: 'resources/img',
                    dest: 'public'

                    //src: 'resources/img/lorenzo-herrera.jpg',
                    //dest: 'public/img/lorenzo-herrera.jpg'
                },{
                    src: 'resources/css',
                    dest: 'public'
                },{
                    src: 'resources/js',
                    dest: 'public'
                },{
                    src: 'node_modules/@fortawesome/fontawesome-free/webfonts',
                    dest: '',
                },{
                    src: 'node_modules/@fortawesome/fontawesome-free/webfonts',
                    dest: 'resources',
                }
            ]
        })
    ],
    resolve: {
        alias: {
            vue: 'vue/dist/vue.esm-bundler.js',
        },
    },
    build: {
        rollupOptions: {
            output: {
                assetFileNames: (assetInfo) => {
                    // Get file extension
                    // TS shows asset name can be undefined so I'll check it and create directory named `compiled` just to be safe
                    let extension = assetInfo.name?.split('.').at(1) ?? 'compiled'

                    // This is optional but may be useful (I use it a lot)
                    // All images (png, jpg, etc) will be compiled within `images` directory,
                    // all svg files within `icons` directory
                    // if (/png|jpe?g|gif|tiff|bmp|ico/i.test(extension)) {
                    //     extension = 'images'
                    // }

                    // if (/svg/i.test(extension)) {
                    //     extension = 'icons'
                    // }

                    // Basically this is CSS output (in your case)
                    return `${extension}/[name].[hash][extname]`
                },
                chunkFileNames: 'js/chunks/[name].[hash].js', // all chunks output path
                entryFileNames: 'js/[name].[hash].js' // all entrypoints output path
            }
        }
    },
});
