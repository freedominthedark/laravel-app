<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        {{--<!--<link href="{{ mix('/css/app.css') }}" rel="stylesheet">-->--}}

        <!-- Scripts -->
        {{-- @vite(['resources/sass/app.scss', 'resources/css/app.css', 'resources/js/app.js']) --}}
        {{--<link rel="stylesheet" href="{{ asset('/public/css/app.css') }}">
        <link rel="stylesheet" href="{{ asset('/public/js/app.js') }}">--}}

{{--        @vite(['resources/css/app.css', 'resources/js/app.js'])--}}
{{--        @vite(['resources/scss/app.scss', 'resources/js/app.js'])--}}
        @vite(['resources/sass/app.scss', 'resources/js/app.js'])
</head>
<body class="antialiased">
<div id="app">
    <div class="container">
        <nav class="navbar navbar-expand bg-light navbar-light">
            <div class="container-fluid">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link menu-link {#{ $mainLink }#} "
                           href=" {{ route('home')  }}">Главная страница</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link menu-link {#{ $articleLink }#}"
                           href="{{ route('article.index')  }}">Каталог статей</a>
                    </li>
                </ul>
                <a target="_blank" class="d-flex justify-content-end " href="https://gitlab.com/freedominthedark/laravel-app">
                    <i class="bi bi-github" style="font-size: 2rem; color: #000000;"></i>
                </a>
            </div>
        </nav>

        @yield('hero')
        @yield('content')
        @yield('vue')
    </div>
</div>
</body>
</html>
